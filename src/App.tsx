import React, { FC } from 'react';
import './App.css';
import Sidebar from './components/Sidebar';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Main from './pages/Main';
import { Chart } from './components/Chart';

const App: FC = () => {
  return (
    <>
      <Router>
        <Sidebar />
        <Switch>
          <Route path="/Main" component={Main} exact></Route>
        </Switch>
        <Chart />
      </Router>
    </>
    
  );
}

export default App;
