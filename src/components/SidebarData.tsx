import { GiSpeedBoat, GiBoatFishing } from 'react-icons/gi';
import { RiNumber1, RiNumber2 } from 'react-icons/ri';
import { SiStreamlit } from 'react-icons/si';
import { BsFillCameraReelsFill } from 'react-icons/bs';

import { SidebarItem } from '../models/SidebarItem';

export const SidebarData: SidebarItem[] = [
    {
        title: 'Zawody',
        path: '/overview',
        icon: <GiSpeedBoat />,
        subnav: [
            {
                title: 'Monako',
                path: '/overview/users',
                icon: <RiNumber1 />
            },
            {
                title: 'Portugalia',
                path: '/overview/revenue',
                icon: <RiNumber2 />
            }
        ]
    },
    {
        title: 'Inne',
        path: '/order',
        icon: <GiBoatFishing />
    },
    {
        title: 'AGH Solar Boat',
        path: '/history',
        icon: <SiStreamlit />
    },
    {
        title: 'Stream',
        path: '/configurations',
        icon: <BsFillCameraReelsFill />
    }
];